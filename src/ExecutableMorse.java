public class ExecutableMorse{
    public static void main(String[]args){
        Lettre n = new Lettre('N');
        assert n.toChar()=='N';
        assert n.toMorse().equals("===_=");
        Lettre a = new Lettre("=_===");
        assert a.toMorse().equals("=_===");
        assert a.toChar()=='A';
        assert a.toNumero() == 0;
        Lettre b = new Lettre("===_=_=_=");
        assert b.toMorse().equals("===_=_=_=");
        assert b.toChar()=='B';
        Lettre n2 = new Lettre('N');
        assert n.equals(n2);
        Texte toto = new Texte("toto");
        assert !(toto.contient(n));
        assert (toto.toMorse()).equals("===___===_===_===___===___===_===_===");
        assert Texte.decode("===___===_===_===___===___===_===_===").equals("TOTO");
        Texte t = new Texte("Salut a tous");
        t.toSon();
    }
}