import java.util.*;

public class Texte {
    /** Cet attribut modélise une liste de lettres */
    private List<Lettre> lettres;
    /** permet de créer un texte à partir d'une chaîne de caractères */
    public Texte(String chaine){
        this.lettres = new ArrayList<>();
        for (int i = 0; i < chaine.length(); ++i){
            this.lettres.add(new Lettre(Character.toUpperCase(chaine.charAt(i))));
        }
    }
    /** permet de créer une chaîne de caractères à partir de la liste de lettres
     * @return une chaîne de caractères
    */
    public String toString(){
        String texte = "";
        for (Lettre lettre : this.lettres){
            texte += lettre.toString();
        }
        return texte;
    }
    /** permet de créer un texte en morse à partir de la liste de lettres
     * @return le texte en morse
    */
    public String toMorse() {
        String texte = "";
        for (Lettre lettre : this.lettres){
            if (lettre.toChar() == ' ')
                texte += "_______";
            else{
                texte += lettre.toMorse() + "___";
            }
        }
        return texte.substring(0, texte.length()-3);
    }
    /** permet de savoir si la lettre placée en paramètre est dans la liste de lettres
     * @param lettre la lettre recherchée dans la liste
     * @return vrai si la liste contient la lettre, false sinon
     */
    public boolean contient(Lettre lettre){
        return this.lettres.contains(lettre);
    }
    /** permet de décoder un texte en morse
     * @param texteEnMorse le texte en morse
     * @return le texte décodé
     */
    public static String decode(String texteEnMorse){
        String texteDecode = "";
        if (texteEnMorse.equals("")){
            return texteEnMorse;
        }
        String[] motsSepares = texteEnMorse.split("_______");
        for (int i = 0 ;i < motsSepares.length; ++i) {
            String[] lettresSeparees = motsSepares[i].split("___");
            for (String lettre : lettresSeparees) {
                texteDecode += new Lettre(lettre).toChar();
            }
            texteDecode += " ";
        }
        return texteDecode.substring(0, texteDecode.length()-1);
    }
    /** permet d'émettre du son pour communiquer en morse */
    public void toSon(){
        Son son = new Son();
        String texteMorse = this.toMorse();
        System.out.println(texteMorse);
        for (int i = 0; i < texteMorse.length(); ++i) {
            if (texteMorse.charAt(i) == '_') {
                son.pause();
            }
            else if (i == 0 && texteMorse.charAt(i+1) == '_') {
                son.tone(100);
            }
            else if (i + 2 < texteMorse.length()) {
                if (texteMorse.charAt(i) == '=' && texteMorse.charAt(i+1) == '=' && texteMorse.charAt(i+2) == '=') {
                    son.tone(300);
                }
                else if (i >= 1 && texteMorse.charAt(i-1) == '_' && texteMorse.charAt(i) == '=' && texteMorse.charAt(i+1) == '_') {
                    son.tone(100);
                }
            }
            if (i == texteMorse.length()-1 && texteMorse.charAt(i) == '=') {
                son.tone(100);
            }
        }
    } 
}