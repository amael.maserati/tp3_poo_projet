import java.util.*;

public class Lettre{
    /** Cet attribut modélise une lettre */
    private char lettre;
    /** Cet attribut modélise une liste de chaînes de caractères */
    private static List<String> alphabetMorse = Arrays.asList("=_===", 
            "===_=_=_=", "===_=_===_=", "===_=_=", "=", "=_=_===_=",
            "===_===_=", "=_=_=_=", "=_=", "=_===_===_===", "===_=_===",
            "=_===_=_=", "===_===", "===_=", "===_===_===", "=_===_===_=",
            "===_===_=_===", "=_===_=", "=_=_=", "===", "=_=_===",
            "=_=_=_===", "=_===_===", "===_=_=_===", "===_=_===_===",
            "===_===_=_=", "_______");
    /** permet de créer une lettre sous forme de caractère
     * @param lettre un caractère
    */
    public Lettre(char lettre){
        this.lettre = lettre;
    }
    /** permet de créer une lettre à partir d'une chaîne de caractères correspondant à une lettre en morse
     * @param morse une lettre en morse
     */
    public Lettre(String morse){
        if (morse.equals("_")){
            this.lettre = (char) 32;
        }
        for (int i = 0; i < alphabetMorse.size(); ++i){
            if (alphabetMorse.get(i).equals(morse)){
                this.lettre = (char)(i + 65);
            }
        }
    }
    /** permet de convertir une lettre en un entier par rapport à sa place dans l'alphabet
     * @return la place de la lettre dans l'alphabet
     */
    public int toNumero(){
        if (this.lettre == ' ')
            return 26;
        return (int)(this.lettre) - 65;
    }
    /** permet de renvoyer un caractère
     * @return un caractère
     */
    public char toChar(){
        return this.lettre;
    }
    /** permet de traduire un texte en morse
     * @return le texte en morse
     */
    public String toMorse(){
        if (this.lettre == ' ')
            return "_______";
        return alphabetMorse.get((int)(this.lettre - 65)); 
    }
    @Override
    public String toString(){
        return String.valueOf(lettre);
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (null == obj) {
            return false;
        }
        if (obj instanceof Lettre) {
            Lettre lettre = (Lettre) obj;
            return lettre.toChar() == this.toChar();
        }
        return false;
    }
}
